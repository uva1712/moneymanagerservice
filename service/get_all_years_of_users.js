const repo = require("../repo/get_all_years_of_users_repo")
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function getYearsOfUserService(req,res){
    try {
        let result  = await repo.getYearsOfUserRepo(req)
        return res.send(responseGenerator.response(code['success'],result,null))
    } catch (error) {
        console.log(error)
        return res.status(500)
    }
}

module.exports = {getYearsOfUserService}