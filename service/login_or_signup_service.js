const signUpRepo = require('../repo/login_or_signup_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');
const getOne = require('../service/get_one_user_service')

async function signUpOrLoginUser(req,res,method){
     console.log("inside service",method);
    let result = await getOne.getOneUserService(req);
    console.log(result)
    if(result === null){
     if(method === 'LOGIN'){
          return res.send(responseGenerator.response(code['UNF'],null,null))
     }else if(method === 'SIGNUP'){
          let result = await signUpRepo.LoginOrSignUpRepo(req,res,method)
          return res.send(responseGenerator.response(code['success'],result,null));
        
     }
    }else{
     if(method === 'LOGIN'){
          let result = await signUpRepo.LoginOrSignUpRepo(req,res,method)
          console.log(result.password,req.body.password);
        if(result.password === req.body.password){
          return res.send(responseGenerator.response(code['success'],null,null))
        }else{
             return res.send(responseGenerator.response(code['invalid_user'],null,null))
        }
     }else if(method === 'SIGNUP'){
          return res.send(responseGenerator.response(code['already_exists'],null,null));
     }
 }
}


module.exports = {signUpOrLoginUser}