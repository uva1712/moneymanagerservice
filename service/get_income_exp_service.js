const getAllRepo = require('../repo/get_income_or_exp_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function getAllIncomeOrExpenseService(req,res,type){
    try {
        let result = await getAllRepo.getAllIncomeOrExpenseRepo(req,type)
        console.log("result od fjj income",result);
        if(result == null){
            return res.send(responseGenerator.response(code['NRF'],null,null))
        }else{
        return res.send(responseGenerator.response(code['success'],result[type],null))
        }
    } catch (error) {
        console.log(error);
    }
}

module.exports = {getAllIncomeOrExpenseService}