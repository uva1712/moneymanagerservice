const repo = require("../repo/get_months_of_users_repo")
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function getMonthsOfUserService(req,res){
    try {
        let result  = await repo.getMonthsOfUserRepo(req)
        return res.send(responseGenerator.response(code['success'],result,null))
    } catch (error) {
        console.log(error)
        return res.status(500)
    }
}

module.exports = {getMonthsOfUserService}