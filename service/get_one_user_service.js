const oneUserRepo = require('../repo/get_one_user_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function getOneUserService(req){
    try {
        return await oneUserRepo.getOneUserRepo(req)
    } catch (error) {
        console.error(error)
    }
}

module.exports = {getOneUserService}