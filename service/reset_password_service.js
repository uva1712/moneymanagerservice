const resetPassRepo = require('../repo/reset_password_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');
const getUser = require('../service/get_one_user_service');

async function resetPassService(req,res){
  try {
      let result = await getUser.getOneUserService(req);
      if(result.password === req.body.tempPassword){
         let updated =  await resetPassRepo.resetPasswordRepo(req);
         if(updated.modifiedCount == 1){
             res.send(responseGenerator.response(code['success'],null,null))
         }else{
             res.send(responseGenerator.response("801",null,null))
         }
      }else{
        res.send(responseGenerator.response(code['invalid_password'],null,null));
      }
  } catch (error) {
      console.log(error);
  }
}

module.exports = {resetPassService}