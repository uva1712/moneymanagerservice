const forgotPassRepo = require('../repo/forgot_password_repo');
const getUser = require('../service/get_one_user_service');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');
const nodemailer = require('nodemailer');
const generator = require('generate-password');

function sendEmail(receiverEmail,autoPassword){
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'moneymanager689@gmail.com',
          pass: 'Money@2021'
        }
      });
      
      var mailOptions = {
        from:'moneymanager689@gmail.com',
        to: receiverEmail,
        subject: 'reset password request',
        text: `Please use the password given below to reset password go to the MoneyManager application and reset your password\n temporary password:${autoPassword}`
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });   
}

function geneRatePassword(){
    var password = generator.generate({
        length: 10,
        numbers: true
    });
    return password;
}
async function forgotPasswordService(req,res){
    try {
        let result = await getUser.getOneUserService(req)
        console.log(result)
        if(result == null){
            return res.send(responseGenerator.response(code['UNF'],null,null))
        }else{
            let password = geneRatePassword();
            sendEmail(req.body.name,password);
            await forgotPassRepo.sendAutoPassword(req,password);
            return res.send(responseGenerator.response(code['success'],null,null));
        }
    } catch (error) {
        console.error(error);
    }
}

module.exports = {forgotPasswordService}