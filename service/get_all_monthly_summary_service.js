const getAllRepo = require('../repo/get_all_monthly_summary_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');


async function getAllMonthlyRecordService(req,res){
    try {
        let result = await getAllRepo.getAllMonthlyRecordRepo(req);
        console.log("result od fjj ",result);
        if(result.length == 0){
            return res.send(responseGenerator.response(code['NRF'],null,null))
        }else{
            return res.send(responseGenerator.response(code['success'],result,null))
        }
    } catch (error) {
        console.error(error)
    }
}

module.exports = {getAllMonthlyRecordService}