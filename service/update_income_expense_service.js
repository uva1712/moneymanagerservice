const updateIncomeRepo = require('../repo/update_income_expense_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function updateIncomeOrExpensesService(req,res,type){
    try {
        let result = await updateIncomeRepo.updateIncomeOrExpensesRepo(req,type);
       console.log(result);
        if(result.modifiedCount>0){
        return res.send(responseGenerator.response(code['success'],null,null))
        }else{
            return res.send(responseGenerator.response(code['not_matched'],null,null))
        }
    } catch (error) {
        console.log(error)
    }
}


module.exports = {updateIncomeOrExpensesService}