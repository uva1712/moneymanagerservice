const incomeRepo = require('../repo/income_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function insertOrUpdateIncomeService(req,res,type){
    try {
        await incomeRepo.insertOrUpdateIncome(req,type);
        return res.send(responseGenerator.response(code['success'],null,null));
    } catch (error) {
        console.log(error)
    }
}


module.exports = {insertOrUpdateIncomeService}