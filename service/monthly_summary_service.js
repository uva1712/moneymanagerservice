const summaryRepo = require('../repo/monthly_summary_repo');
const responseGenerator = require('../response/responsebody');
const code = require('../response/responseCode.json');

async function getSummaryService(req,res,type){
    try {
        let result = await summaryRepo.monthlyOrYearlySummaryRepo(req,type);
        return res.send(responseGenerator.response(code['success'],result,null))
    } catch (error) {
        console.log(error);
    }
}

module.exports = {getSummaryService}