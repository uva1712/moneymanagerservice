function response(responseCode,responseValue,responseError){
    return {
        header:{
            code:responseCode
        },
        body:{
            value:responseValue,
            errors:responseError
        }
    }
}

module.exports = {response}