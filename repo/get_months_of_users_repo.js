const operation = require("../databasemanagement/dboperations")

async function getMonthsOfUserRepo(req){
    try {
        return await operation.getMonthsOfUser(req)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {getMonthsOfUserRepo}