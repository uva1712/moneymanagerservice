const operation = require("../databasemanagement/dboperations")

async function getYearsOfUserRepo(req){
    try {
        return await operation.getYearsOfUser(req)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {getYearsOfUserRepo}