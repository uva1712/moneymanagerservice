const operation = require('../databasemanagement/dboperations')

async function getAllIncomeOrExpenseRepo(req,type){
    try {
        return await operation.getAllIncomeOrExpenseBYId(req,type);
    } catch (error) {
        console.log(error)
    }
}

module.exports = {getAllIncomeOrExpenseRepo}