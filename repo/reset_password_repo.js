const operation = require('../databasemanagement/dboperations')

async function resetPasswordRepo(req){
        try {
            return await operation.resetPassword(req);
        } catch (error) {
            console.error(error);
        }
}

module.exports = {resetPasswordRepo}