const operation = require('../databasemanagement/dboperations');

async function updateIncomeOrExpensesRepo(req,type){
    try {
        return await operation.updateIncomeOrExpensesOperation(req,type);
    } catch (error) {
        console.log(error);
    }
}

module.exports = {updateIncomeOrExpensesRepo}