const operation = require('../databasemanagement/dboperations');

async function getAllMonthlyRecordRepo(req){
    try {
        return await operation.getAllMonthlyRecordOperation(req)
    } catch (error) {
        console.error(error)
    }
}


module.exports = {getAllMonthlyRecordRepo}