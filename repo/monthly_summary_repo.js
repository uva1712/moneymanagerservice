const operation = require('../databasemanagement/dboperations')

async function monthlyOrYearlySummaryRepo(req,type){
     try {
         return await operation.getMonthlyOrYearlySummaryBYId(req,type)
     } catch (error) {
         
     }
}

module.exports = {monthlyOrYearlySummaryRepo}