const operation = require('../databasemanagement/dboperations')


async function LoginOrSignUpRepo(req,res,method){
    try{
     return await operation.signUpLoginOperation(req,method)
    } catch (e){
        return e;
    }
}

module.exports = {LoginOrSignUpRepo}