const operation = require('../databasemanagement/dboperations')

async function getOneUserRepo(req,res){
    try{
        return await operation.getOneUser(req)
       } catch (e){
           return e;
       }
}

module.exports = {getOneUserRepo}