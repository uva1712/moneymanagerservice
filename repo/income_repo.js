const operation = require('../databasemanagement/dboperations');

async function insertOrUpdateIncome(req,type){
    try {
        return await operation.insertIncomeExpenseOperation(req,type)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {insertOrUpdateIncome}