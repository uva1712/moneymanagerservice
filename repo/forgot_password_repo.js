const operation = require('../databasemanagement/dboperations')

async function sendAutoPassword(req,password){
    try{
        return await operation.autoPassword(req,password)
       } catch (e){
           return e;
       }
}

module.exports = {sendAutoPassword}