const {MongoClient} = require('mongodb')

async function dbConnection(callBacks){
    const uri = `mongodb+srv://uva:UVA@cluster0.1mo1g.mongodb.net/test`;
    const client = new MongoClient(uri);
    try{
      let connection =  await client.connect();
      return  await callBacks(connection);
    } catch (e){
        console.error(e);
    } finally {
       await client.close();
    }
}

module.exports = {dbConnection};