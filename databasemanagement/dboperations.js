const con = require('./dbconnection');

async function signUpLoginOperation(req,method){
   return await con.dbConnection(async (connection)=>{
        console.log("qwefghhtvccnhbvgfcnnb",method)
        try{
                    if(method === 'LOGIN'){
                        return await connection.db("moneymanager").collection("users").findOne({"email":req.body.email});
                    }else if(method === 'SIGNUP'){
                        try{
                        await connection.db("moneymanager").collection("users").insertOne(req.body);
                        return await connection.db("moneymanager").collection("users").findOne({"email":req.body.email});
                } catch (e){
                    console.error(e);
                }
            }
        } catch (e) {
            console.error(e)
        }
      });
}



async function getOneUser(req){
    return await con.dbConnection(async (connection)=>{
        try{
        return await connection.db("moneymanager").collection("users").findOne({"email":req.body.email})
        } catch (e){
            console.log(e)
        }
    });
}


async function autoPassword(req,password){
    return await con.dbConnection(async (connection)=>{
        try{
            return  await connection.db("moneymanager").collection("users").updateOne({ email: req.body.email }, { $set: {password:`${password}`} });
        } catch (e){
            console.error(e);
        }
    })
}

async function resetPassword(req){
    return await con.dbConnection(async (connection)=>{
         try {
             console.log(req.body.password)
             return await connection.db("moneymanager").collection("users").updateOne({ email: req.body.email }, { $set: {password:`${req.body.password}`} });
         } catch (error) {
             console.error(error)
         }
    })
}

async function updateIncomeOrExpensesOperation(req,type){
    return await con.dbConnection(async (connection)=>{
        try {
            let id = req.body.uId+req.body.year+req.body.month;
            console.log(type);
            let findDocument = `${type}._id`
            let fieldToBeUpdated = `${type}.$.amount`
           let result =  await connection.db("moneymanager").collection(type).updateOne({ "_id": id, [findDocument]:req.body.category},
            { $inc: { [fieldToBeUpdated] : req.body.amount } });
            await monthlySummaryOperation(req,type);
            await yearlySummaryOperation(req,type);
            return result;
        } catch (error) {
            console.error(error)
        }
    })
}

async function getYearsOfUser(req,res){
    return await con.dbConnection(async (connection)=>{
        try {
            console.log("before")
            console.log(req.query.uId,req.query.year)
            console.log("after")
          const cursor =  await connection.db("moneymanager").collection("yearlysummary")
          .find({'id': req.params.uId}).map((record)=>record.year);
          let data = cursor.toArray();
          return data;
        } catch (error) {
          console.error(error)
        }
    })
  }
  

async function getMonthsOfUser(req,res){
  return await con.dbConnection(async (connection)=>{
      try {
          console.log("before")
          console.log(req.query.uId,req.query.year)
          console.log("after")
        const cursor =  await connection.db("moneymanager").collection("monthlysummary")
        .find({'id': req.query.uId,"year":req.query.year}).map((record)=>record.month);
        let data = cursor.toArray();
        return data;
      } catch (error) {
        console.error(error)
      }
  })
}

async function getMonthlyOrYearlySummaryBYId(req,type){
    return await con.dbConnection(async (connection)=>{
        try {
            let id = req.params.id;
          return await  connection.db("moneymanager").collection(type).findOne({"_id":id});
        } catch (error) {
            console.error(error)
        }
    })
}

async function getAllIncomeOrExpenseBYId(req,type){
    return await con.dbConnection(async (connection)=>{
        try {
            let id = req.params.id;
            console.log(id)
          return await  connection.db("moneymanager").collection(type).findOne({"_id":id});
        } catch (error) {
            console.error(error)
        }
    })
}
async function insertIncomeExpenseOperation(req,type){
    return await con.dbConnection(async (connection)=>{
      try {
          let id = req.body.uId+req.body.year+req.body.month;
          let result = await  connection.db("moneymanager").collection(type).findOne({"_id":id});
          if(result == null){
              try {
                  let result = await connection.db("moneymanager").collection(type).insertOne({
                      '_id': req.body.uId+req.body.year+req.body.month,
                      'uId':req.body.uId,
                      [`${type}`]:[{'_id':req.body.category,...req.body}]
                  })
                 await monthlySummaryOperation(req,type);
                 await yearlySummaryOperation(req,type);
                 return result;
              } catch (error) {
                console.error(error)
              }
          }else{
              try {
               await connection.db("moneymanager").collection(type)
                .updateOne({'_id': req.body.uId+req.body.year+req.body.month},{$push:{[`${type}`]:{'_id':req.body.category,...req.body}}});
                await monthlySummaryOperation(req,type);
                await yearlySummaryOperation(req,type);
                return result;
              } catch (error) {
                  console.log(error)
              }   
          }
         
      } catch (error) {
        console.error(error)
      }
      })
}

async function getAllMonthlyRecordOperation(req){
    return await con.dbConnection(async (connection)=>{
        try {
            const cursor =  await connection.db("moneymanager").collection("monthlysummary")
            .find({'id': req.body.uId,"year":req.body.year});
            let result = cursor.toArray();
            return result;
        } catch (error) {
            console.error(error);
        }
    })
}

async function yearlySummaryOperation(req,type){
    return await con.dbConnection(async (connection)=>{
        try {
            let id = req.body.uId+req.body.year;
            let result = await  connection.db("moneymanager").collection("yearlysummary").findOne({"_id":id});
            if(result == null){
                try {
                    let result = await connection.db("moneymanager").collection("yearlysummary").insertOne({
                        '_id': req.body.uId+req.body.year,
                        'id':req.body.uId,
                        'totalIncome':type === 'income'?req.body.amount:0,
                        'totalExpenses':type === 'expenses'?req.body.amount:0,
                        'year':req.body.year
                        }
                )
                return result;
                } catch (error) {
                  console.error(error)
                }
            }else{
                try {
                    if(type === 'income'){
                        return await connection.db("moneymanager").collection("yearlysummary")
                        .updateOne({'_id': id},{$inc:{totalIncome:req.body.amount}});
                    } else if(type === 'expenses'){
                        return await connection.db("moneymanager").collection("yearlysummary")
                        .updateOne({'_id': id},{$inc:{totalExpenses:req.body.amount}});
                    }   
                } catch (error) {
                    console.log(error)
                }
                  
            }
           
        } catch (error) {
          console.error(error)
        }
})
}
async function monthlySummaryOperation(req,type){
    return await con.dbConnection(async (connection)=>{
        try {
            let id = req.body.uId+req.body.year+req.body.month;
            let result = await  connection.db("moneymanager").collection("monthlysummary").findOne({"_id":id});
            if(result == null){
                try {
                    let result = await connection.db("moneymanager").collection("monthlysummary").insertOne({
                        '_id': req.body.uId+req.body.year+req.body.month,
                        'id':req.body.uId,
                        'totalIncome':type === 'income'?req.body.amount:0,
                        'totalExpenses':type === 'expenses'?req.body.amount:0,
                        'availableBalance':req.body.amount||0,
                        'month':req.body.month,
                        'year':req.body.year
                        }
                )
                return await result;
                } catch (error) {
                  console.error(error)
                }
            }else{
                try {
                    if(type === 'income'){
                        return await connection.db("moneymanager").collection("monthlysummary")
                        .updateOne({'_id': req.body.uId+req.body.year+req.body.month},{$inc:{totalIncome:req.body.amount,availableBalance:req.body.amount}});
                    } else if(type === 'expenses'){
                        return await connection.db("moneymanager").collection("monthlysummary")
                        .updateOne({'_id': req.body.uId+req.body.year+req.body.month},{$inc:{totalExpenses:req.body.amount,availableBalance:-req.body.amount}});
                    }   
                } catch (error) {
                    console.log(error)
                }
                  
            }
           
        } catch (error) {
          console.error(error)
        }
    })
}
module.exports = {signUpLoginOperation,getOneUser,getMonthsOfUser,getYearsOfUser,autoPassword,getAllMonthlyRecordOperation,resetPassword,getMonthlyOrYearlySummaryBYId,insertIncomeExpenseOperation,getAllIncomeOrExpenseBYId,updateIncomeOrExpensesOperation}