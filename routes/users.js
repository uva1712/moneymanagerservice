var express = require('express');
var router = express.Router();
var signUpService = require('../service/login_or_signup_service');
const forgotPass = require('../service/forgot_password_service');
const resetPass =require('../service/reset_password_service');
const insertIOrE = require('../service/income_service');
const getIncomeOrExp = require('../service/get_income_exp_service');
const summaryService = require('../service/monthly_summary_service');
const updateIncomeOrExp = require('../service/update_income_expense_service');
const getAllSummaryService = require('../service/get_all_monthly_summary_service');
const getMonthsService = require('../service/get_months_of_users_service');
const getYearsService = require('../service/get_all_years_of_users');


/* GET users listing. */
router.get('/uva', function(req, res, next) {
  res.send('respond with uva resource');
});

router.post("/login",(req,res)=>{
  signUpService.signUpOrLoginUser(req,res,"LOGIN");
})


router.post("/signup",(req,res)=>{
  signUpService.signUpOrLoginUser(req,res,"SIGNUP");
})


router.post("/requestpassword",(req,res)=>{
  forgotPass.forgotPasswordService(req,res)
})


router.post('/resetpassword',(req,res)=>{
    resetPass.resetPassService(req,res);
})

router.post('/income',(req,res)=>{
  insertIOrE.insertOrUpdateIncomeService(req,res,'income');
})

router.post('/expenses',(req,res)=>{
  insertIOrE.insertOrUpdateIncomeService(req,res,'expenses');
})

router.get('/income/:id',(req,res)=>{ 
  getIncomeOrExp.getAllIncomeOrExpenseService(req,res,'income');
})

router.get('/expenses/:id',(req,res)=>{
  getIncomeOrExp.getAllIncomeOrExpenseService(req,res,'expenses');
})

router.get('/monthlysummary/:id',(req,res)=>{
 summaryService.getSummaryService(req,res,'monthlysummary');
})

router.get('/yearlysummary/:id',(req,res)=>{
  summaryService.getSummaryService(req,res,'yearlysummary');
 })

router.post('/updateincome',(req,res)=>{
  updateIncomeOrExp.updateIncomeOrExpensesService(req,res,'income');
 })
 
router.post('/updateexpenses',(req,res)=>{
  updateIncomeOrExp.updateIncomeOrExpensesService(req,res,'expenses');
})

router.post('/getAllMonthlySummary',(req,res)=>{
  getAllSummaryService.getAllMonthlyRecordService(req,res);
})

router.get('/getAllMonths',(req,res)=>{
  getMonthsService.getMonthsOfUserService(req,res);
})

router.get('/getAllYears/:uId',(req,res)=>{
  getYearsService.getYearsOfUserService(req,res);
})

module.exports = router;
